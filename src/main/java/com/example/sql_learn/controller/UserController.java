package com.example.sql_learn.controller;

import com.example.sql_learn.entity.Student;
import com.example.sql_learn.service.StudentService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: Holoong
 * @Date: 2020/10/15 20:05
 */

@RestController
@RequestMapping("/student")

public class UserController {

    @Autowired
    private StudentService studentService;

    @GetMapping("list")
    public List list(Student student){
        List<Student> list = studentService.list();
        return list;
    }

    //学生列表

}

