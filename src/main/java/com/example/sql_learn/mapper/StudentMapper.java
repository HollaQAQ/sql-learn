package com.example.sql_learn.mapper;

import com.example.sql_learn.entity.Student;

import java.util.List;

/**
 * @Author: Holoong
 * @Date: 2020/10/15 20:09
 */
public interface StudentMapper {

    //新增学生
    int add(Student student);

    //根据id查询学生
    Student getById(Integer id);

    //根据id删除学生
    int delete(Integer id);

    //修改学生信息
    int update(Student student);

    //根据id修改学生年龄

    //学生列表
    List<Student> list();


}
