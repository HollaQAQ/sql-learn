package com.example.sql_learn.crud;

import java.sql.*;
import java.util.Scanner;

/**
 * @Author: Holoong
 * @Date: 2020/10/17 14:04
 */
public class updateById {
    public static void main(String[] args) {
        ResultSet rs = null;
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            //驱动加载
            Class.forName("com.mysql.cj.jdbc.Driver");
            //连接数据库
            connection = DriverManager.getConnection
                    ("jdbc:mysql://127.0.0.1:3306/sql-learn?serverTimezone=UTC&useSSL=true&" +
                            "characterEncoding=utf-8&user=" +
                            "root&password=123456");
            System.out.println("数据库创建连接成功！！！");
            //输入学号
            System.out.print("请输入修改学生的学号：");
            Scanner input1 = new Scanner(System.in);
            int id = input1.nextInt();
            String sql1 = "select * from student where id = " + id;
            //得到statement对象
            statement = connection.prepareStatement(sql1);
            //执行sql得到结果集
            rs = statement.executeQuery();
            //处理结果集
            if (rs.next()) {

                System.out.println("修改成功！");

            }
            else {
                System.out.println("修改失败！！！");
                return;
            }
            statement.executeUpdate();
            //输入年龄
            System.out.print("请输入修改的年龄：");
            Scanner input2 = new Scanner(System.in);
            int age=input2.nextInt();
            String sql = "update student set age=? where id=?";
            //得到statement对象
            statement = connection.prepareStatement(sql);
            //执行sql得到结果集
            statement =  connection.prepareStatement(sql);
            //处理结果集,插入数据
            statement.setInt(1,age);
            statement.setInt(2,id);
            //关闭资源
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}


