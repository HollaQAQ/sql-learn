package com.example.sql_learn.crud;

import java.sql.*;
import java.util.Scanner;

/**
 * @Author: Holoong
 * @Date: 2020/10/17 13:41
 */
public class insert {
    public static void main(String[] args) {
        ResultSet rs = null;
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            //驱动加载
            Class.forName("com.mysql.cj.jdbc.Driver");
            //连接数据库
            connection = DriverManager.getConnection
                    ("jdbc:mysql://127.0.0.1:3306/sql-learn?serverTimezone=UTC&useSSL=true&" +
                            "characterEncoding=utf-8&user=" +
                            "root&password=123456");
            System.out.println("数据库创建连接成功！！！");
            //输入学号
            System.out.print("请输入修改学生的学号：");
            Scanner input1 = new Scanner(System.in);
            int id = input1.nextInt();
            //输入年龄
            System.out.print("请输入修改学生的年龄：");
            Scanner input2 = new Scanner(System.in);
            int age = input2.nextInt();
            //输入姓名
            System.out.print("请输入修改学生的姓名：");
            Scanner input3 = new Scanner(System.in);
            String name = input3.next();

            //sql语句
            String sql = "insert into student(id,age,name) values(?,?,?)";
            //得到statement对象
            statement = connection.prepareStatement(sql);
            //执行sql得到结果集
            statement =  connection.prepareStatement(sql);
            //处理结果集,插入数据
            statement.setInt(1, id);
            statement.setInt(2, age);
            statement.setString(3, name);
            statement.executeUpdate();
            System.out.println("插入成功！");
            //关闭资源
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}