package com.example.sql_learn.crud;

/**
 * @Author: Holoong
 * @Date: 2020/10/17 13:23
 */


import java.sql.*;

public class list{
    public static void main(String[] args) {

        ResultSet rs = null;
        //创建PreparedStatement实例对象list，用于sql语句执行
        PreparedStatement list = null;
        Connection connection = null;
        try {
            //驱动加载
            Class.forName("com.mysql.cj.jdbc.Driver");
            //连接数据库
            connection = DriverManager.getConnection
                    ("jdbc:mysql://127.0.0.1:3306/sql-learn?serverTimezone=UTC&useSSL=true&" +
                            "characterEncoding=utf-8&user=" +
                            "root&password=123456");
            System.out.println("数据库创建连接成功！！！");
            System.out.println("id " + " age " + "name");
            //sql语句
            String sql = "select * from student";
            //得到list对象
            list = connection.prepareStatement(sql);
            //执行sql得到结果集
            rs = list.executeQuery();
            //处理结果集
            while (rs.next()) {
                System.out.print(rs.getInt(1) + " ");
                System.out.print(rs.getInt(2) + "  ");
                System.out.println(rs.getString(3) + " ");
            }
            //关闭资源
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (list != null) {
                try {
                    list.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}