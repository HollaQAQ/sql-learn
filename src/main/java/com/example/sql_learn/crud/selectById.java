package com.example.sql_learn.crud;

import org.w3c.dom.ls.LSInput;

import java.sql.*;
import java.util.Scanner;

/**
 * @Author: Holoong
 * @Date: 2020/10/17 13:47
 */
public class selectById {
    public static void main(String[] args) {

        ResultSet rs = null;
        //创建PreparedStatement实例对象statement，用于sql语句执行
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            //驱动加载
            Class.forName("com.mysql.cj.jdbc.Driver");
            //连接数据库
            connection = DriverManager.getConnection
                    ("jdbc:mysql://127.0.0.1:3306/sql-learn?serverTimezone=UTC&useSSL=true&" +
                            "characterEncoding=utf-8&user=" +
                            "root&password=123456");
            System.out.println("数据库创建连接成功！！！");
            //输入学号
            System.out.print("请输入查询学生的学号：");
            Scanner input = new Scanner(System.in);
            int id = input.nextInt();
            //int id = 123;
            //sql语句
            String sql = "select * from student where id = " + id;
            //得到statement对象
            statement = connection.prepareStatement(sql);
            //执行sql得到结果集
            rs = statement.executeQuery();
            //处理结果集
            if (rs.next()) {
                System.out.println("id: " + rs.getInt(1));
                System.out.println("age: " + rs.getInt(2));
                System.out.println("name: " + rs.getString(3));

            }
            else {
                System.out.println("查询失败！！！");
            }
            //关闭资源
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

