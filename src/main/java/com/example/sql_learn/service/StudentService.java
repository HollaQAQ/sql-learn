package com.example.sql_learn.service;

import com.example.sql_learn.entity.Student;
import com.example.sql_learn.mapper.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @Author: Holoong
 * @Date: 2020/10/15 20:08
 */

@Service
public class StudentService {

    private StudentMapper studentMapper;

    @Autowired
    public List<Student> list(){
        return studentMapper.list();
    }
}

