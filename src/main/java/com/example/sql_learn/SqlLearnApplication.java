package com.example.sql_learn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqlLearnApplication {

    public static void main(String[] args) {
        SpringApplication.run(SqlLearnApplication.class, args);
    }

}
